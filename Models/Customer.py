class Customer:
    def __init__(self, content):
        self.ID = None
        self.name = None
        self.phone = None

        if 'id' in content:
            self.ID = content['id']
        if 'name' in content:
            self.name = content['name']
        if 'phone' in content:
            self.phone = content['phone']
