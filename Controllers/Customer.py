import DBManager
import Models.Customer
import sqlite3
from flask import jsonify


def customer_new(content):
    db = DBManager.create_connection("crocosoft")
    obj = Models.Customer.Customer(content)
    query = "INSERT INTO Customer (Name, Phone) VALUES (?, ?);"

    customer = (obj.name, obj.phone)
    cur = db.cursor()
    try:
        cur.execute(query, customer)
        db.commit()
        db.close()
    except sqlite3.IntegrityError as e:
        db.close()
        return "Error occured : {}".format(e)

    return cur.lastrowid


def customer_update(content):
    db = DBManager.create_connection("crocosoft")
    obj = Models.Customer.Customer(content)
    query = "UPDATE Customer SET "

    query_parameters = []
    count = 0
    if obj.name is not None:
        query_parameters.append(obj.name)
        query += "name = ? , "
        count += 1

    if obj.phone is not None:
        query_parameters.append(obj.phone)
        query += "phone = ? , "
        count += 1

    query = query[:-2]

    query += "WHERE id = ? ;"
    query_parameters.append(obj.ID)
    print(query)

    cur = db.cursor()
    try:
        cur.execute(query, query_parameters)
        db.commit()
        db.close()
    except sqlite3.IntegrityError as e:
        db.close()
        return "Error occured : {}".format(e)

    return cur.lastrowid

def customer_delete(ID):
    db = DBManager.create_connection("crocosoft")
    query = "DELETE FROM Customer WHERE ID = ? ;"

    customer = (ID)
    cur = db.cursor()
    try:
        cur.execute(query, customer)
        db.commit()
        db.close()
    except sqlite3.IntegrityError as e:
        db.close()
        return "Error occured : {}".format(e)

    return "Success"

def customer_get(content):
    db = DBManager.create_connection("crocosoft")
    obj = Models.Customer.Customer(content)
    query = "SELECT * FROM Customer"

    has_filter = False
    if obj.ID is not None or obj.name is not None or obj.phone is not None :
        has_filter = True

    if has_filter :
        query += " WHERE "

    if obj.ID is not None : query += " ID = {} AND ".format(obj.ID)
    if obj.name is not None : query += " Name LIKE '%{}%' AND ".format(obj.name)
    if obj.phone is not None : query += " Phone LIKE '%{}% ;".format(obj.phone)

    cur = db.cursor()
    try:
        cur.execute(query)
        data = cur.fetchall()
        db.close()
        return data     
    except sqlite3.IntegrityError as e:
        db.close()
        return "Error occured : {}".format(e)

    return cur.lastrowid