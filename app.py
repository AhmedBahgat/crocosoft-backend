from flask import Flask, request, jsonify
from markupsafe import escape
import DBManager
import Controllers.Customer

# initialize the db connection
db = DBManager.create_connection("crocosoft")

# create the app
app = Flask(__name__)

# routes
@app.route('/customer/new', methods=['POST'])
def customer_new():
    content = request.json
    return {"result": Controllers.Customer.customer_new(content)}


@app.route('/customer/update', methods=['POST'])
def customer_modify():
    content = request.json
    return {"result": Controllers.Customer.customer_update(content)}

@app.route('/customer/delete/<ID>', methods=['PATCH'])
def customer_delete(ID):
    return {"result": Controllers.Customer.customer_delete(escape(ID))}

@app.route('/customer/get', methods=['POST'])
def customer_get():
    content = request.json
    return {"result": Controllers.Customer.customer_get(content)}