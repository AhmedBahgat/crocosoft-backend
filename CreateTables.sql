-------------------Vechicle---------------
CREATE TABLE "Vechicle" (
	"ID"	INTEGER,
	"Number"	TEXT NOT NULL,
	"Type"	TEXT NOT NULL,
	"Capacity"	INTEGER NOT NULL,
	"Status"	TEXT,
	PRIMARY KEY("ID" AUTOINCREMENT)
)

---------------Customer-------------------
CREATE TABLE "Customer" (
	"ID"	INTEGER NOT NULL,
	"Name"	TEXT NOT NULL,
	"Phone"	TEXT NOT NULL,
	PRIMARY KEY("ID" AUTOINCREMENT)
)

-----------------Booking------------------
CREATE TABLE "Booking" (
	"ID"	INTEGER,
	"Car"	INTEGER NOT NULL,
	"Customer"	INTEGER NOT NULL,
	"CreatedAt"	INTEGER NOT NULL,
	"HiringDate"	INTEGER NOT NULL,
	"ReturningDate"	INTEGER NOT NULL,
	"Amount"	REAL NOT NULL,
	PRIMARY KEY("ID" AUTOINCREMENT)
)